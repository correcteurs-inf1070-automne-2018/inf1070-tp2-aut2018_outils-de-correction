
## Git

Les contributions sont faites au moyen de requête d'intégration vers le dépôt. À
cet effet, voyez la spécification [c590][] pour la bonne rédaction d'un message
de validation. Notez que les codes (`[+]`, `[-]`, ...) sont optionnels, mais le
reste est obligatoire.

[c590]: https://gist.github.com/sim590/7eba3f6d3d6831c64db1b8b0facea1ec

## Style d'écriture du code

Ci-après les différentes contraintes d'écriture du code pour une meilleure
uniformité:

* Code rédigé en **anglais**.
* Commentaires rédigés en **français**
* Largeur des lignes: `120` caractère max.
* Aucune **tabulation** n'est permise.
* **L'indentation** fait `2` espaces par unité.

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

