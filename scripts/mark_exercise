#!/bin/bash

smark=
ACGT_LETTERS=(A C G T)
ADN_MARKERS=(DOM-A CONS-6 PAL-11 ALT-C10 MUT-13)

mark_question() {
  printf "($1) ${YELLOW}${BOLD}Quelle note attribuer (??/$2) ?${NO_COLOUR} "
  read smark

  while ! grep -qE "^[0-9]+$" <<< $smark || (( $smark > $2 )); do
    printf "Entrer un nombre entre 0 et $2. " >&2
    read smark
  done
}
attribute_mark() { sed -i "/$1\\s*:\\s\\+.*\\/$3/s/:.*$/: $2\/$3/" "$eval_file"; }
already_marked() { grep -E "$1\\s*:\\s+(10|[0-9])\\/$2" "$eval_file"; }
remark() {
  if already_marked "$1" "$2"; then
    print_yesno_question "La note est déjà attribuée. Refaire ?"
    read on
    [[ $on =~ ou?i? ]] && return 0 || return 1
  fi
}

setup_test_dir() {
  td=$(mktemp -d)
  scriptname=$1 ; shift
  tp2_exercise_dir="$tp2_dir/$(basename "$PWD")"

  testscript=$td/$scriptname.test
  solscript=$td/$scriptname.sol
  cp "$solutions_dir/$scriptname" $solscript
  # Déplacement du script de l'étudiant en le convertissant à UTF-8
  # Autrement, y'a des bugs de caractères avec `sed` / `grep`.
  if file -i $scriptname | grep -qioP 'charset=\K.*utf.*?;?' ; then
    install -m 755 $scriptname $testscript
  else
    iconv -c -f LATIN1 -t UTF8 $scriptname >$testscript
    chmod 755 $testscript
  fi
  dos2unix $testscript
  [[ "$@" != "" ]] && cp -rt $td "$@"
  echo $td
}

test_tops() {
  scriptname=tops
  td=$(setup_test_dir $scriptname)
  cd $td
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    ./$scriptname.$s >$s.out
  done
  print_diff_display $scriptname
  diff test.out sol.out
  return $tops_rc
}


test_uptime() {
  scriptname=quand
  td=$(setup_test_dir $scriptname)
  cd $td
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    ./$scriptname.$s >$s.out
  done
  print_diff_display $scriptname
  diff test.out sol.out
  return $tops_rc
}

test_compresse() {
  scriptname=decompresser
  test_files="{extrait*.txt.{gz,xz,bz2},extrait4,README.md}"
  test_files_src="$tp2_dir/compresse/$test_files"
  td=$(setup_test_dir $scriptname)
  cd $td
  for s in test sol ; do
    eval cp -rt . "$test_files_src"
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for e in $(eval echo $test_files); do
      print_step "$scriptname.$s $e"
      yes n | ./$scriptname.$s $e
      outfile="${e%.*}"
      [[ $outfile =~ [^.]+\. ]] || outfile=$outfile.out
      sha1sum $outfile >>$s.out 2>/dev/null
    done
    rm -f *.txt extrait4.out
    eval sha1sum "$test_files" >>$s.out 2>/dev/null
  done
  print_diff_display $scriptname
  diff <(sort test.out) <(sort sol.out)
}

test_adn() {
  scriptname=adn
  test_files="$tp2_dir/adn/adn*"
  td=$(setup_test_dir $scriptname $test_files)
  number_of_adn_files=5
  cd $td
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for (( i = 1; i <= $number_of_adn_files; i++ )); do
      a="adn${i}.txt"
      ./$scriptname.$s $a >>$s.out.$i
      for l in "${ACGT_LETTERS[@]}"; do
        sed -n "s/.*nombre\\s\\+de\\s\\+$l\\s*:\\s*\\([0-9]\\+\\).*/\\1/Ip" $s.out.$i | tr -d '[:blank:]' >$l.$s.$i
      done
      for m in "${ADN_MARKERS[@]}"; do
        sed -n "s/.*$m.*\\(oui\\|non\\).*/\\1/Ip" $s.out.$i | tr -d '[:blank:]' | tr '[:upper:]' '[:lower:]' >$m.$s.$i
      done
    done
  done
  print_diff_display $scriptname
  for (( i = 1; i <= $number_of_adn_files; i++ )); do
    for l in "${ACGT_LETTERS[@]}"; do
      d=$(diff $l.test.$i $l.sol.$i) || printf "${YELLOW}Nombre de ${l} (adn$i.txt)${NO_COLOUR}:\n$d\n"
      rm $l.test.$i $l.sol.$i
    done
    for m in "${ADN_MARKERS[@]}"; do
      d=$(diff $m.test.$i $m.sol.$i) || printf "${YELLOW}Présence du marqueur ${m} (adn$i.txt)${NO_COLOUR}:\n$d\n"
      rm $m.test.$i $m.sol.$i
    done
  done
}

test_verif() {
  scriptname=verif
  test_files="$mark_template_dir/fichiers-de-test/tp2.md.*"
  td=$(setup_test_dir $scriptname $test_files)
  cd $td
  number_of_tp2_md_files=4
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for (( i = 1; i <= ${number_of_tp2_md_files}; i++ )); do
      cp tp2.md.$i tp2.md
      ./$scriptname.$s >$s.out.$i
    done
  done
  for (( i = 1; i <= ${number_of_tp2_md_files}; i++ )); do
    type_f=
    case $i in
      1 )
        type_f="vierge"
        ;;
      2 )
        type_f="avec erreurs et espaces"
        ;;
      3 )
        type_f="sans erreurs, mais des espaces"
        ;;
      4 )
        type_f="sans erreurs, sans espaces"
        ;;
    esac
    d=$(diff <(sort test.out.$i) <(sort sol.out.$i))
    if (( $? != 0 )); then
      printf "${YELLOW}Différences pour tp2.md.${i} (${type_f})${NO_COLOUR}:\n$d\n"
    fi
  done
}

test_stats() {
  scriptname=genstats
  statsdir=tmp.0WywIn3Oeb0WywIn3Oeb
  stats_archive="$mark_template_dir/fichiers-de-test/stats.tar.gz"
  patch="$mark_template_dir/patches/gendir.patch"
  td=$(setup_test_dir $scriptname $tp2_dir/stats/{gendir,getstats,genhist.gnuplot,dates.txt})
  cd $td
  tar -xzf "$stats_archive" 2>/dev/null
  mv stats $statsdir
  patch gendir $patch
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    ./$scriptname.$s "$(./gendir)" exemple.$s.png
  done
  print_diff_display $scriptname
  diff <(sha1sum exemple.test.png) <(sha1sum exemple.sol.png)
}

test_debug() {
  scriptname=debug
  export -f setup_test_dir
  export tp2_dir solutions_dir
  exercisedir="$tp2_dir/debug"
  td=$(xargs -d '\n' bash -c 'setup_test_dir "$@"' _ $scriptname \
       < <(find $exercisedir -mindepth 1 -maxdepth 1 ! -name debug))
  cd $td
  printf "${YELLOW}${BLINK}ATTENTION${NO_COLOUR}: ces programmes peuvent bloquer.\n%s\n" \
    "Utiliser ctrl+c pour continuer, le cas échéant..."
  trap 'pkill debug' SIGINT
  for s in test sol ; do
    [[ "$s" == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    print_step "$scriptname.$s *brute.md"
    ./$scriptname.$s *brute.md
    print_step "$scriptname.$s *truand.md"
    ./$scriptname.$s *truand.md
    print_step "$scriptname.$s */test.md"
    ./$scriptname.$s */test.md
    find . -type f -name \*.pdf -exec echo {} \; -exec rm {} \; >$s.out
  done
  trap SIGINT
  diff <(sort test.out) <(sort sol.out)
}

test_curl() {
  scriptname=telecharge
  td=$(setup_test_dir $scriptname)
  cd $td
  for s in test sol ; do
    [[ "$s" == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    ./$scriptname.$s pdf
    find . -name \*.pdf -type f -exec sha1sum {} \; -exec rm {} \; >$s.out
    rm -rf pdf
  done
  diff <(sort test.out) <(sort sol.out)
}

mark_exercise() {
  exercise="$1"
  rc=
  cd $exercise || print_error "Format des répertoires non-respectés. Tentative d'utiliser le répertoire courant."
  case "$exercise" in
    tops)
      exercise_text="Exercice 1"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_tops
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    uptime)
      exercise_text="Exercice 2"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_uptime
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    compresse)
      exercise_text="Exercice 3"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_compresse
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    adn)
      exercise_text="Exercice 4"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_adn
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    verif)
      exercise_text="Exercice 5"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_verif
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    stats)
      exercise_text="Exercice 6"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_stats
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    debug)
      exercise_text="Exercice 7"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_debug
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
    curl)
      exercise_text="Exercice 8"
      total_pts=10
      remark "$exercise_text" $total_pts || return
      test_curl
      rc=$?
      continue_or_shell . "Répertoire de test"
      mark_question "$exercise" $total_pts
      attribute_mark "$exercise_text" $smark $total_pts
      ;;
  esac
  return $rc
}

# vim: set sts=2 ts=2 sw=2 tw=120 et :

