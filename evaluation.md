# Évaluation du travail pratique 2

Note totale: ??/100

## Identification

- Cours      : Utilisation et administration des systèmes informatiques
- Sigle      : INF1070
- Session    : Automne 2018
- Groupe     : `<numéro du groupe>`
- Enseignant : `<nom de votre enseignant>`
- Auteur     : `<votre nom>` (`<votre code permanent>`)
- Auteur     : `<votre nom>` (`<votre code permanent>`)

## Qualité de la remise: ??/20

Commentaires s'il y a lieu

## Exercice 1: ??/10

Commentaires s'il y a lieu

## Exercice 2: ??/10

Commentaires s'il y a lieu

## Exercice 3: ??/10

Commentaires s'il y a lieu

## Exercice 4: ??/10

Commentaires s'il y a lieu

## Exercice 5: ??/10

Commentaires s'il y a lieu

## Exercice 6: ??/10

Commentaires s'il y a lieu

## Exercice 7: ??/10

Commentaires s'il y a lieu

## Exercice 8: ??/10

Commentaires s'il y a lieu
