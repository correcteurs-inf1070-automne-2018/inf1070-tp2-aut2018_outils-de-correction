
# INF1070 - Outils de correction - Session Automne 2018

Ce dépôt contient les outils de correction pour le cours INF1070 de l'Université
du Québec à Montréal.

Le présent dépôt contient les routines de correction du Travail Pratique #2 de
la session automne 2018.

## Outils

L'outil de correction de ce dépôt est un script assitant le correcteur dans le
parcours des différents numéros du travail pratique à corriger. Les différents
points suivants sont comptés parmis les fonctionnalités de l'outil:

* Préparer le répertoire avec le fichier d'évaluation.
* Itérer à travers tous les exercices.
* Affichage coloré afin de faciliter la lecture de l'information.
* Démarrer la correction à un exercice donné (`-e`, voir `--help`).
* Des options pour réduire le nombre de questions (`-E`, `-C`).
* Gérer le signal d'interruption par l'utilisateur (`CTRL+C`). Le script écrit
  automatiquement la note dans le fichier d'évaluation.
* Affiche la différence entre la sortie du programme de l'étudiant et celle de
  la solution au format standard de *patch*.

## Méthodologie de contribution

Voir le fichier [`CONTRIBUER.md`](./CONTRIBUER.md).

## Auteur

Simon Désaulniers (<sim.desaulniers@gmail.com>)

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

